import 'package:flutter/material.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/model/movies_details_response.dart';
import 'package:movieappbloc/repos/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieDetailsBloc {
  MovieRepository _repository = MovieRepository();
  BehaviorSubject<MovieDetailsResponse> _subject =
      BehaviorSubject<MovieDetailsResponse>();

  getMovieDetail(int id) async {
    MovieDetailsResponse response = await _repository.getDetailsMovie(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<MovieDetailsResponse> get subject => _subject;
}

final movieDetailBloc = MovieDetailsBloc();
