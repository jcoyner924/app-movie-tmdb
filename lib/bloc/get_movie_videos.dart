import 'package:flutter/material.dart';
import 'package:movieappbloc/model/cast_response.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/model/video_response.dart';
import 'package:movieappbloc/repos/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieVideosBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<VideoResponse> _subject =
  BehaviorSubject<VideoResponse>();

  getMovieVideo(int id) async {
    VideoResponse response = await _repository.getVideosMovie(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<VideoResponse> get subject => _subject;
}

final movieVideoBloc = MovieVideosBloc();
