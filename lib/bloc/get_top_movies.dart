import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/repos/repository.dart';
import 'package:rxdart/rxdart.dart';

class TopMoviesBloc{

  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<MovieResponse> _subject = BehaviorSubject<MovieResponse>();

  getTopMoviesPlaying() async {
    MovieResponse response = await _repository.getTopPlayingMovies();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject => _subject;
}
final topMoviesBloc = TopMoviesBloc();