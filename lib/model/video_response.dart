import 'package:movieappbloc/model/video.dart';

class VideoResponse {
  final List<Video> videos;
  final String error;

  VideoResponse(this.videos, this.error);

  VideoResponse.fromJson(Map<String, dynamic> json)
      : videos = (json[''] as List).map((e) => new Video.fromJson(e)).toList(),
        error = "";

  VideoResponse.withError(String errorVal)
      : videos = List(),
        error = errorVal;
}
