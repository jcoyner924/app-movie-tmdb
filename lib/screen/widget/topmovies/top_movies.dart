import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movieappbloc/bloc/get_top_movies.dart';
import 'package:movieappbloc/model/movie.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:page_indicator/page_indicator.dart';

class TopMovies extends StatefulWidget {
  @override
  _TopMoviesState createState() => _TopMoviesState();
}

class _TopMoviesState extends State<TopMovies> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    topMoviesBloc..getTopMoviesPlaying();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
      stream: topMoviesBloc.subject.stream,
      builder: (context, AsyncSnapshot<MovieResponse> snapshotTopMovies) {
        if (snapshotTopMovies.hasData) {
          if (snapshotTopMovies.data.error != null &&
              snapshotTopMovies.data.error.length > 0) {
            return _buildErrorWidget(snapshotTopMovies.data.error);
          }
          return _buildTopMoviesWidget(snapshotTopMovies.data);
        } else if (snapshotTopMovies.hasError) {
          return _buildErrorWidget(snapshotTopMovies.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String data) {
    return Center(
      child: Column(
        children: [Text(data)],
      ),
    );
  }

  Widget _buildTopMoviesWidget(MovieResponse data) {
    List<Movie> movies = data.movies;
    if (movies.length == 0) {
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [Text("No Movies")],
        ),
      );
    } else
      return Container(
          height: 220.0,
          child: CarouselSlider.builder(
            itemCount: movies.length,
            itemBuilder: (context, index) => Stack(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 220.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          image: NetworkImage(
                              "https://image.tmdb.org/t/p/original" +
                                  movies[index].backPoster))),
                ),
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            Colors.black12.withOpacity(1.0),
                            Colors.white12.withOpacity(0.0)
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          stops: [0.0, 0.9])),
                ),
                Positioned(
                  bottom: 30.0,
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Column(
                      children: [
                        Text(movies[index].title, style: TextStyle(color: Colors.white, fontSize: 16.0),)
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: Icon(FontAwesomeIcons.playCircle, color: Colors.yellow,size: 40.0,),
                  bottom: 0,
                  left: 0,
                  top: 0,
                  right: 0,
                )
              ],
            ),
            options: CarouselOptions(
                autoPlay: true,
                viewportFraction: 0.9,
                height: 200.0,
                enlargeCenterPage: false),
          ));
  }
}
